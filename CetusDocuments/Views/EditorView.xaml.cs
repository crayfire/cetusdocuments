﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CetusDocuments
{
    /// <summary>
    /// Interaktionslogik für EditorView.xaml
    /// </summary>
    public partial class EditorView : UserControl
    {
        public EditorView()
        {
            InitializeComponent();
            List<MergeFields> varList = new List<MergeFields>()
            {
                new MergeFields()
                {
                Anspruchsteller = "Sebastian Heldt",
                AnspruchstellerNachname = "Heldt",
                Anwalt = "Rechtsanwalt Hans-Joachim Blömeke",
                AuftragDatum = "12.06.2019",
                Auftraggeber = "Anspruchsteller",
                Besichtiger = "Sebastian Heldt",
                Besichtigungsdatum = "13.06.2019",
                Besichtigungsort = "Huttenstr. 27, 10553 Berlin",
                Besichtigungszeit = "10:30",
                Betreff = "Haftpflichtschaden",
                Datum = "14.06.2019",
                Fahrzeughersteller = "Hyundai",
                Fahrzeugmodell = "i30 SW",
                Gutachtendatum = "13.06.2019",
                Gutachtennummer = "201906001",
                KennzeichenAST = "B-SH 5804",
                KennzeichenVN = "MOL-XX 666",
                Reparaturdauer = "3-4",
                Reparaturfirma = "CSB Schimmel Automobile",
                Schadennummer = "2019-06-12345/K0-1",
                Schadentag = "25.05.2019",
                Versicherung = "HUK Coburg",
                Versicherungsnehmer = "Max Thomas Müller",
                VersicherungsnehmerNachname = "Müller",
                VersicherungsNummer = "123/456/789",
                Wiederbeschaffungsdauer = "21"
                }

            };
            this.editor.Document.MailMergeDataSource.ItemsSource = varList;
        }
    }
}
