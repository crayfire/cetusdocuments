﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CetusDocuments
{
    public class MergeFields
    {
        public string Datum { get; set; }
        public string Gutachtennummer { get; set; }
        public string Gutachtendatum { get; set; }
        public string Betreff { get; set; }
        public string Versicherung { get; set; }
        public string VersicherungsNummer { get; set; }
        public string Schadennummer { get; set; }
        public string KennzeichenAST { get; set; }
        public string KennzeichenVN { get; set; }
        public string Schadentag { get; set; }
        public string AuftragDatum { get; set; }
        public string Auftraggeber { get; set; }
        public string Besichtigungsdatum { get; set; }
        public string Besichtigungszeit { get; set; }
        public string Besichtigungsort { get; set; }
        public string Besichtiger { get; set; }
        public string Anspruchsteller { get; set; }
        public string AnspruchstellerNachname { get; set; }
        public string Versicherungsnehmer { get; set; }
        public string VersicherungsnehmerNachname { get; set; }
        public string Reparaturfirma { get; set; }
        public string Anwalt { get; set; }
        public string Reparaturdauer { get; set; }
        public string Wiederbeschaffungsdauer { get; set; }
        public string Fahrzeughersteller { get; set; }
        public string Fahrzeugmodell { get; set; }


    }
}
